import {
  Component,
  EventEmitter,
  Input,
  ElementRef
} from "@angular/core"
import {
  Validators,
  FormBuilder,
  ControlGroup,
  NgControl,
  ControlValueAccessor
} from '@angular/common'
import * as _ from 'lodash'
import * as moment from 'moment'
import {MD_INPUT_DIRECTIVES} from '@angular2-material/input';

@Component({
  selector: "date",
  template: require('!jade!./date.jade')(),
  styles: [require('./date.scss').toString()],
  directives: [MD_INPUT_DIRECTIVES]
})

export class DateType implements ControlValueAccessor {
  @Input() placeholder: string
  @Input() initDate: string
  @Input() modelFormat: string = 'YYYY-MM-DD'

  isOpened: boolean
  date: any
  minDate: any
  maxDate: any

  dayNames: Array<string>
  public days: Array<Object>
  currentDate: any
  viewValue: string
  dateValue: string
  isFirstMonth: boolean = false
  isLastMonth: boolean = false
  cannonical: number

  initValue: string
  model: any
  form: ControlGroup
  onChange: EventEmitter<any> = new EventEmitter()
  onTouched: any

  private _fb: FormBuilder
  private _control: NgControl
  private _el: ElementRef

  constructor(
    fb: FormBuilder,
    control: NgControl,
    el: ElementRef
  ) {
    this._fb = fb
    this._control = control
    this._el = el
    this._initControl()
  }

  private _initControl() {
    this._control.valueAccessor = this
    this.form = this._fb.group({
      model: ['']
    })
    this.isOpened = false
    this.date = moment()
    this.generateDayNames()

    this.minDate = moment()
    this.maxDate = _.cloneDeep(this.minDate).add(CONFIG.CALENDAR_MAX_DAYS || 365, 'd')

    this.initMouseEvents()
  }

  ngOnInit() {
    this._initModel()
  }
  ngAfterViewInit() {
    this.initializeValue()
  }

  private _initModel() {
    
  }

  open(event: MouseEvent) {
    console.log(event)
    this.isOpened = true
    this.generateCalendar(this.currentDate)
  }

  close() {
    this.isOpened = false
  }

  isSelected(date) {
    let selectedDate = moment(date.day + '.' + date.month + '.' + date.year, 'DD.MM.YYYY')
    return selectedDate.toDate().getTime() === this.cannonical
  }

  public selectDate(e, date): void {
    e.preventDefault()
    if (this.isSelected(date)) return

    let selectedDate = moment(date.day + '.' + date.month + '.' + date.year, 'DD.MM.YYYY')
    this.setValue(selectedDate)
    this.close()
  }

  private setValue(value: any): void {
    let val = moment(value, this.modelFormat || 'YYYY-MM-DD');
    // this.viewValue = val.format(this.viewFormat || 'Do MMMM YYYY');

    this.viewValue = val.calendar(null, {
      sameDay: '[Today]',
      nextDay: '[Tomorrow]',
      nextWeek: 'Do MMMM YYYY',
      lastDay: 'Do MMMM YYYY',
      lastWeek: 'Do MMMM YYYY',
      sameElse: 'Do MMMM YYYY'
    })

    this.onChange.emit(val.format())

    this.currentDate = val

    this.cannonical = val.toDate().getTime()
  }

  private initializeValue(): void {
    setTimeout(() => {
      if (!this.initDate) {
        this.setValue(moment().format(this.modelFormat))
      } else {

        this.date = moment(this.initDate, this.modelFormat)
        this.currentDate = this.date
        this.setValue(this.date)
      }

    });
  }

  private generateCalendar(date): void {
    let lastDayOfMonth = date.endOf('month').date();
    let month = date.month();
    let year = date.year();
    let n = 1;
    let firstWeekDay = null;


    this.isFirstMonth = this.minDate.month() == month && this.minDate.year() == year;
    this.isLastMonth = this.maxDate.month() == month && this.maxDate.year() == year;


    this.dateValue = date.format('MMMM YYYY');
    this.days = [];

    firstWeekDay = date.set('date', 1).day();

    if (firstWeekDay !== 1) {
      n -= (firstWeekDay + 6) % 7;
    }

    for (let i = n; i <= lastDayOfMonth; i += 1) {

      let date = moment(i + '.' + (month + 1) + '.' + year, 'DD.MM.YYYY');

      if (i > 0) {

        let enabled = date.isBetween(_.cloneDeep(this.minDate).add(-1, 'd'), _.cloneDeep(this.maxDate).add(1, 'd'));
        this.days.push({ day: i, month: month + 1, year: year, enabled: enabled });

      } else {
        this.days.push({ day: null, month: null, year: null, enabled: false });
      }
    }
  }
  private generateDayNames(): void {
    this.dayNames = []
    let date = moment('2015-06-01')
    for (let i = 0; i < 7; i += 1) {
      this.dayNames.push(date.format('ddd'))
      date.add('1', 'd')
    }
  }

  private initMouseEvents(): void {
    let body = document.getElementsByTagName('body')[0]

    body.addEventListener('click', (e) => {
      if (!this.isOpened || !e.target) return
      if (this._el.nativeElement !== e.target && !this._el.nativeElement.contains(e.target)) {
        this.close();
      }
    }, false);
  }

  writeValue(value) {
    console.log(value)
    if (!value) return
    this.initDate = moment(value, 'X').format(this.modelFormat)
  }
  registerOnChange(fn): void {
    this.onChange.subscribe(fn)
  }
  registerOnTouched(fn): void {
    this.onTouched = fn
  }
}